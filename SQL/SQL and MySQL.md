# SQL 

> reference 【3小时学会MySQL 初学入门完整教程[2019] - 完整翻译】https://www.bilibili.com/video/BV1iJ411m7Fj?vd_source=c13765adc65e1127b9523161e34f9a99

 

### sql_hr

### sql_inventory

### sql_invoicing

### sql_store



``` sql
use sql_store;

select * 
from customers
where cistomer_id = 1
order by first_name
-- comments
-- order of above is important!!!
-- no difference by using capital or lowercase 

select first_name, last_name, points, points + 10
from customers

-- if too long you can split
select 
	last_name,
	first_name,
	points,
	(points + 10) * 2 as 'discont factor'
from customers

select DISTINCT state
from customer
-- get unique (no duplicate) for state

-- and 优先级比 or 高

select *
from customers
where not (birth_date <= '1990 - 01- 01' or points> 10000  )

select *
from customers
where state in ('va', 'fl', 'ga') 

select *
from customers
where points between 1000 and 3000

select *
from customers
where last_name like 'b%' -- start with b
'_____y' -- 5 char before 'y'

select *
from customers
where last_name regexp '^field'

-- ^ begin
-- $ end
-- [a-h]
-- [abc]

select *
from customers
where phone is null

select *
from customers
order by state desc, first_name desc

select *
from customers
limit 3 -- print 3 lines
limit 6,3 -- print line 7,8,9

select *
from orders o 
join customers c 
	on o.customer_id = c.customer_id
-- if variable shows in both tables, you should add table name.variable

-- implict join (not recommend)

-- outer join
select *
from orders o 
left join customers c 
	on o.customer_id = c.customer_id
-- print out all cunstomers whether they have ordered or not  

select *
from orders o 
join customers c
	using (customer_id)

 
  
```







``` sql
select email, count(email) as e 
from person
where e > 1
group by email having e > 1
```

